Simple script for merging historical analyse data produced by [Sonar QA Platform](http://www.sonarsource.org/).

USAGE

1. Prepare migrating rules file
2. Pass it to the script `migrate.py rule.json`

RULES FILE FORMAT

    :::json
        [{
          "sources": [{
            "id": [6523, 13054], // int or list
            "db": {
              "name": "sonar",
              "user": "java",
              "password": "java",
              "address": "localhost:5432"
            }
          }],
          "target": {
            "id": 11236,
            "db": {
              "name": "sonar_migrated",
              "user": "java",
              "password": "java",
              "address": "localhost:5432"
            }
          }
        }]

KNOWN ISSUES/LIMITATIONS

1. Supports olny postgres for now
2. May produce some minor historical data loss from source project
3. Tested on sonar 3.3.2 and 3.4 only

#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.schema import CreateTable
from sqlalchemy.sql import select, join
from urllib import quote_plus
from itertools import *
import urllib2
import json
from pprint import pprint
from copy import copy


CONNECT_STRING = \
  'postgresql+psycopg2://%(user_name)s:%(passwd)s@%(server)s/%(database)s'
def createConnect(**credentials):
  engine = create_engine(
      CONNECT_STRING % credentials,
      echo=False,
      encoding="utf-8",
      assert_unicode=False,
      convert_unicode=False
      )

  metadata  = MetaData(bind = engine)
  return engine.connect()


ROOT_PROJECT1_SQL = "SELECT * from projects where root_id=%(projectId)s"
ROOT_PROJECT1_SQL = "SELECT * from projects where root_id in (%(subsql)s)"
PROJECT_INSERT_SQL = """INSERT INTO projects 
            (name, description, enabled, scope, qualifier, kee, root_id, 
             language, copy_resource_id, long_name, person_id, created_at) 
     VALUES (%(name)s, %(description)s, %(enabled)s, %(scope)s, 
             %(qualifier)s, %(kee)s, %(root_id)s, %(language)s, 
             %(copy_resource_id)s, %(long_name)s, %(person_id)s, 
             %(created_at)s) RETURNING id"""

PROJECT_UPDATE_SQL = """UPDATE projects set root_id = %(root_id)s where id=%(id)s"""


SNAPSHOT_INSERT_SQL = """INSERT INTO snapshots 
            (created_at, project_id, parent_snapshot_id, status, islast, scope,
             qualifier, root_snapshot_id, version, path, depth, root_project_id, 
             period1_mode, period1_param, period1_date, period2_mode, 
             period2_param, period2_date, period3_mode, period3_param, 
             period3_date, period4_mode, period4_param, period4_date,
             period5_mode, period5_param, period5_date, build_date,
             purge_status) 
     VALUES (%(created_at)s, %(project_id)s, %(parent_snapshot_id)s, %(status)s, 
             %(islast)s, %(scope)s, %(qualifier)s, %(root_snapshot_id)s, 
             %(version)s, %(path)s, %(depth)s, %(root_project_id)s, 
             %(period1_mode)s, %(period1_param)s, %(period1_date)s, 
             %(period2_mode)s, %(period2_param)s, %(period2_date)s, 
             %(period3_mode)s, %(period3_param)s, %(period3_date)s,
             %(period4_mode)s, %(period4_param)s, %(period4_date)s,
             %(period5_mode)s, %(period5_param)s, %(period5_date)s,
             %(build_date)s, %(purge_status)s) RETURNING id"""

SNAPSHOT_UPDATE_SQL = """UPDATE snapshots set 
  root_snapshot_id = %(root_snapshot_id)s, 
  parent_snapshot_id = %(parent_snapshot_id)s
  WHERE id = %(id)s"""
  
SNAPSHOT_SOURCES_INSERT_SQL = """INSERT INTO snapshot_sources 
            (snapshot_id, data) 
     VALUES (%(snapshot_id)s, %(data)s) RETURNING id"""

SNAPSHOT_SOURCES_INSERT_SQL = """INSERT INTO snapshot_sources 
            (snapshot_id, data) 
     VALUES (%(snapshot_id)s, %(data)s) RETURNING id"""

MEASURE_DATA_INSERT_SQL = """INSERT INTO measure_data 
            (snapshot_id, data) 
     VALUES (%(snapshot_id)s, %(data)s) RETURNING id"""

PROJECT_MEASURES_INSERT_SQL = """INSERT INTO project_measures 
            (value, metric_id, snapshot_id, rule_id, rules_category_id, 
             text_value, tendency, measure_date, project_id, alert_status, 
             alert_text, url, description, rule_priority, characteristic_id,
             variation_value_1, variation_value_2, variation_value_3, 
             variation_value_4, variation_value_5, person_id) 
     VALUES (%(value)s, %(metric_id)s, %(snapshot_id)s, %(rule_id)s, 
             %(rules_category_id)s, %(text_value)s, %(tendency)s, 
             %(measure_date)s, %(project_id)s, %(alert_status)s, 
             %(alert_text)s, %(url)s, %(description)s, %(rule_priority)s, 
             %(characteristic_id)s, %(variation_value_1)s, 
             %(variation_value_2)s, %(variation_value_3)s,%(variation_value_4)s, 
             %(variation_value_5)s, %(person_id)s) RETURNING id"""

def fetch(conn, sql, arg):
  print "---> %s" % (sql % arg)
  result = []
  for val in conn.execute(sql, arg):
    result.append(dict(val))
  return result

def update(conn, sql, arg):
  #print "---> %s" % (sql % arg)
  return conn.execute(sql, arg)

branched = {}

def getProjectsByLevel(conn, projectId, result):
  root = fetch(conn, "SELECT * from projects where id=%s", projectId)[0]
  if len(root['kee'].split(':')) == 3:
    branched[projectId] = True
  elif len(root['kee'].split(':')) == 2:
    branched[projectId] = False
  else:
    raise Exception()

  portion = []
  for val in conn.execute(generateSubProjectSQL(projectId, len(result))):
    portion.append(dict(val))
  if not len(portion):
    return result

  result.append(portion)
  return getProjectsByLevel(conn, projectId, result)

def generateSubProjectSQL(projectId, depth):
  if not depth:
    return "SELECT * from projects where root_id=%(projectId)s" % {"projectId": projectId};
  else:
    subsql = "SELECT id from projects where root_id=%(projectId)s" % {"projectId": projectId}
    for i in xrange(depth-1):
      subsql = "SELECT id from projects where root_id in (%(subsql)s)" % {"subsql": subsql}
    subsql = "SELECT * from projects where root_id in (%(subsql)s)" % {"subsql": subsql}

    return subsql

def skipBranch(projectId, kee):
    arr = kee.split(':')

    if branched[projectId]:
      if len(arr) == 4:
        return ":".join((arr[0], arr[1], arr[3]))
      elif len(arr) == 3:
        return ":".join((arr[0], arr[1]))
    else:
      if len(arr) in (2, 3):
        return kee
    raise Exception("unknown format! " + kee)

def replaceBranch(projectId, kee, branch):
    arr = kee.split(':')

    if branched[projectId]:
      if len(arr) == 4:
        return ":".join((arr[0], arr[1], branch, arr[3]))
      elif len(arr) == 3:
        return ":".join((arr[0], arr[1], branch))
    else:
      if len(arr) == 2:
        return kee + ":" + branch
      elif len(arr) == 3:
        return ":".join((arr[0], arr[1], branch, arr[2]))
    raise Exception("unknown format!")

def processProjectTree(conn, projectId):
  result = reduce(lambda x, y: x + y, getProjectsByLevel(conn, projectId, []))
  d = {}
  for obj in result:
    correctKee = skipBranch(projectId, obj['kee'])
    print branched, obj['kee'], '->', correctKee
    if correctKee in d:
      #pprint(map(lambda x: x['kee'], result))
      raise Exception('kee %s already found! %s' % (correctKee, d.keys()))
    d[correctKee] = obj
  return d


# default -> 13054
# master -> 6523

# iwa 3466

# 1 -> 11236
# 3466 -> 13054


#source = 4709
#destination = 11236


def merge(p1, p2):
  c1 = createConnect(**p1['conn'])
  c1.autocommit=True

  c2 = createConnect(**p2['conn'])
  c2.autocommit=True

  source = p1['id']
  destination = p2['id']

  toCreate = []
  mapping = {source: destination}
  snapshot_mapping = {}

  src = processProjectTree(c1, source)
  dst = processProjectTree(c2, destination)

  for k, o1 in src.items():
    if k in dst:
      o2 = dst[k]
      mapping[o1['id']] = o2['id']

      # need to update some values if source resource is older
      if o1['created_at'] and o2['created_at'] and (o1['created_at'] < o2['created_at']):
	update(c2, "UPDATE projects set created_at=%s WHERE id=%s", (o1['created_at'], o2['id']))
    else:
      toCreate.append(o1)

  # insert projects from toCreate and keep identificators
  for obj in toCreate:
    obj['kee'] = replaceBranch(source, obj['kee'], "<default>")
    project_id = update(c2, PROJECT_INSERT_SQL, obj).fetchone()[0]
    mapping[obj['id']] = project_id
    obj['id'] = project_id

  for obj in toCreate:
      if obj['root_id']:
	  obj['root_id'] = mapping[obj['root_id']]
	  update(c2, PROJECT_UPDATE_SQL, obj)

  # copy snapshots from src to destination and save mapping
  # ------------------------------------------------------------------------------

  def projectExists(conn, projectId):
    return len(fetch(conn, "select * from projects where id=%s", projectId)) > 0

  for snapshot in fetch(c1, "SELECT * from snapshots where root_project_id=%s", source):
    if snapshot['root_project_id'] in mapping:
      c = copy(snapshot)
      if snapshot['project_id'] in mapping:
	c['project_id'] = mapping[snapshot['project_id']]
      elif not projectExists(c1, c['project_id']):
	c['project_id'] = 0
	print "WARNING! Project %s not exists!" % c['project_id']
      else:
	pprint(snapshot)
	pprint(mapping)
	pprint(snapshot['project_id'])
	pprint(snapshot['root_project_id'])
	raise Exception('!!!!!')

      c['root_project_id'] = mapping[snapshot['root_project_id']]
      c['islast'] = False

      snapshot_id = update(c2, SNAPSHOT_INSERT_SQL, c).fetchone()[0]
      snapshot_mapping[snapshot['id']] = snapshot_id
    else:
      pprint(snapshot)
      pprint(mapping)
      pprint(snapshot['project_id'])
      pprint(snapshot['root_project_id'])
      raise Exception('!!')
  
  for snapshot in fetch(c2, "SELECT * from snapshots where root_project_id=%s", source):
    parent = None
    root = None
    path = None

    if snapshot['parent_snapshot_id'] and snapshot['parent_snapshot_id'] in snapshot_mapping:
      parent = snapshot_mapping[snapshot['parent_snapshot_id']]
    if snapshot['root_snapshot_id'] and snapshot['root_snapshot_id'] in snapshot_mapping: 
      root = snapshot_mapping[snapshot['root_snapshot_id']]

    if root or parent:
      c = copy(snapshot)
      c['root_snapshot_id'] = root
      c['parent_snapshot_id'] = parent
      c['islast'] = False
      update(c2, SNAPSHOT_UPDATE_SQL, c)
    
  # copy snapshot_sources applying snapshot mapping
  # ------------------------------------------------------------------------------
  for source_id in snapshot_mapping.keys():
    for snapshot_source in fetch(c1, "SELECT * from snapshot_sources where snapshot_id = %s", source_id):
      c = copy(snapshot_source)
      c['snapshot_id'] = snapshot_mapping[snapshot_source['snapshot_id']]
      update(c2, SNAPSHOT_SOURCES_INSERT_SQL, c)

  # copy measure_data appying snapshot mapping and keep mapping
  # ------------------------------------------------------------------------------
  for source_id in snapshot_mapping.keys():
    for measure_data in fetch(c1, "SELECT * from measure_data where snapshot_id = %s", source_id):
      c = copy(measure_data)
      c['snapshot_id'] = snapshot_mapping[measure_data['snapshot_id']]
      update(c2, MEASURE_DATA_INSERT_SQL, c)

  # copy project_measures applying snapshot & measure_data mappings
  for source_id in snapshot_mapping.keys():
    for project_measure in fetch(c1, "SELECT * from project_measures where snapshot_id = %s", source_id):
      c = copy(project_measure)
      c['snapshot_id'] = snapshot_mapping[project_measure['snapshot_id']]
      
      if project_measure['project_id']:
	c['project_id'] = mapping[project_measure['project_id']]
      update(c2, PROJECT_MEASURES_INSERT_SQL, c)
	
  # METRICS mapping?!!
  pprint(mapping)
  print 'mapping -> %s' % len(mapping)
  print 'src -> %s' % len(src)
  print 'dst -> %s' % len(dst)
  
  c1.close()
  c2.close()

if __name__ == "__main__":
  from sys import argv, exit
  from json import loads
  import numbers
  
  if len(argv) < 2:
    print "Usage %s [options.json]" % argv[0]
    exit(128)
  
  with open(argv[1]) as f:
    data = loads(f.read())

  def convert(d, projectId = None):
    return {
      'id': projectId if projectId else d['id'],
      'conn': {
        'database': d['db']['name'],
        'server': d['db']['address'],
        'user_name': d['db']['user'],
        'passwd': d['db']['password']
      }
    }
  
  # convert to obsolete format
  for rule in data:
    target = convert(rule['target'])
    
    for s in rule['sources']:
      ids = s['id']
      ids = [ids] if isinstance(ids, numbers.Number) else ids
      for sourceId in ids:
        source = convert(s, sourceId)
        merge(source, target)
  